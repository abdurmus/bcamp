# System Infrastructure Bootcamp - Database Case-1 <h1>


## Case 1. Sıfırdan yazılması planlanan bir projenin alt yapısında dağıtık NoSQL bir veritaban gereksinimi oluşmuştur. Bu proje üzerinden hem Key-Value hem de SQL sorguları ile çalışılması beklenmektedir. Altyapıda konumlandırılacak NoSQL veritabanı sisteminin yüksek erişilebilir, dağıtık yapıda çalışabilen ve gerektiğinde hızlı olarak ölçeklendirilebilen bir sistem olması gerekmektedir. Bu proje alt yapısında kullanılacak NoSQL veritabanı sistemini Couchbase teknolojisi ile tasarlanması gerekmektedir. <h2>

Burada Docker container üzerinde 3 farklı Couchbase server kurulumu gerçekleştireceğiz. Windows ortamı üzerinde Docker Desktop kullarak bunu yapmayı planlıyorum.

İlk olarak Docker Deskop kurulumu gerekmektedir.**

>Docker Deskop kurulum linki aşağıdaki gibidir.

    https://www.docker.com/products/docker-desktop

> Ardından kurulum ve servis başlatma sonrası Command Prompt veya Powershell'den devam edebiliriz.

Couchbase kurulumu için gerekli docker komutlarını sırasıyla çalıştıralım.

    docker run -d --name db1 couchbase
    docker run -d --name db2 couchbase
    docker run -d --name db3 -p 8091-8094:8091-8094 -p 11210:11210 couchbase

> Burada db1,db2,db3 adında 3 farklı couchbase db oluşturulacaktır. db 3 üzerinden http://localhost:8091/ui/index.html linkinden erişim sağlanmaktadır.

![alt tag](https://cloudflare-ipfs.com/ipfs/QmR5foTC6U8fZErhcDxK4MYwJUjzy9nPRBLRbYinp8mi3n)

Şu adımlarla Cluster kurulumunu tamamlayalım.

    Setup New Cluster > Cluster name : abdurmus > Create Admin Username: Administrator Password: password > Next > Accept Terms & Conditions > Configure Disk > Hostname : 127.0.0.1 > Data Disk Path : /opt/couchbase/var/lib/couchbase/data > Save & Finish

> Bu adımları tamamladıktan sonra login ekranında kullanıcı bilgilerimizi girip login olalım.

Bu aşamada Cluster kurulumunu tamamladık. 
Multi cluster kurulumu için ise makine IP adreslerine ihtiyacımız var.

> IP adresini öğrenmek için db1 container'ına **docker exec** komutu ile CLI üzerinden bağlanıyoruz. 

    #ifconfig 

![alt tag](https://cloudflare-ipfs.com/ipfs/QmV5UBMtKYEmyeqwFizEdV2Qmyy5nWrh5xNy9vyjSdiGt2)

> Aynı komutu db2 içinde çalıştırıp ip bilgisini öğrenelim. 

Sonuç olarak; 

    db1 : 172.17.0.2
    db2 : 172.17.0.3
    db3 : 172.17.0.4

>Şimdi ekledğimiz db2 ve db1'i cluster'a dahil edeceğiz.  Aşağıdaki adımları izleyerek yapalım;

    Servers > Add Server
> db1 ,db2 ip adreslerini sırasıyla ekleyelim.

![alt tag](https://cloudflare-ipfs.com/ipfs/Qmdq3Km2CL6wKUs5g1QbvaV4KgxaU4Le3TU2xdb4yukbF4)

> Eklendikten sonra üstteki Rebalance butonuyla cluster'ın kurulumunu tamamlayalım. _Bu işlem biraz sürebilir._

![alt tag](https://cloudflare-ipfs.com/ipfs/QmTvcTmEunMNb3obgU66mEuBYhUY5jMdEmhkmdGEZYL5P5)

Cluster kurulumu tamamlandı örnek bir bucket oluşturup kontrol edelim. 

    Buckets > New Bucket > beer-sample > Add Bucket

> Oluştuktan sonra böyle görülmeli;

![alt tag](https://cloudflare-ipfs.com/ipfs/QmazVug4db9SksbyHohcbayeBLp2RvwyZoZpHxoUKrE8LV) 

> Ardından Dashboard sekmesinden gelerek 3 node'da olan durum grafiklerini gözlemleyelim.

![alt tag](https://cloudflare-ipfs.com/ipfs/QmRL33GGF9EVn3UHnknEu183VHyTQbdGX1Crrn1ZDfuDU8) 

Bu aşamada artık cluster'a bağlı 3 node başarıyla çalışmaktadır.


Case 2 - ( { }) - yetişemedi.
